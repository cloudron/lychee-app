[0.1.0]
* Initial version

[0.1.1]
* Update to base image 0.10.0
* Enable imagick extension for image rotation

[0.1.2]
* Use Lychee 3.1.6

[0.1.3]
* Update description

[0.1.4]
* Cleanup php sessions

[0.2.0]
* Better apache configs

[1.0.0]
* Use new Cloudron base image

[1.1.0]
* Update to Lychee 3.2.8

[1.2.0]
* Update to Lychee 3.2.9

[1.2.1]
* Update to Lychee 3.2.10

[1.2.2]
* Update to Lychee 3.2.11
* Add description overlay and takestamps overlay in addition to Exif. Closes #167
* Add Setting to remove script execution time limit during imports. Fixes #177

[1.2.3]
* Update to Lychee 3.2.12
* Add usage of exiftool to get exif tags from camera #189
* Fixes image size missing from about #188

[1.2.4]
* Update to Lychee 3.2.13
* Add "unjustified" layout
* Improve Diagnostics page
* Fixes #194, #196, #205, #208

[1.2.5]
* Update Lychee to 3.2.14
* Add primary key to settings table (#221)
* Add git source commit/branch/repo to Diagnostics page
* Use better lens tags from exiftool if present (if exiftool is enabled) (#235)
* Accept larger input from exiftool
* Make photo/album IDs more consistent
* Add fullscreen support to album and photo views (#228)
* Use sortingAlbums and sortingPhotos even if logged out.
* Hide passwords. Add password confirmation.

[1.3.0]
* Update Lychee to 3.2.15
* improve stability when getting bad EXIF data ( LycheeOrg/Lychee-Laravel#205 )
* ignore bad shutter data (#240)
* switch the git commit format number to only 7 characters in Diagnostics
* takedate format string (#215, #256)
* add setting to allow public search (#262)
* wrong version number displayed in Lychee (#268)

[1.3.1]
* Update Lychee to 3.2.16
* hides lychee version number by default (LycheeOrg/Lychee-Laravel#282)

[2.0.0]
* Update Lychee to 4.0.6
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.0.6)
* This is a major update of Lychee. The app has been rewritten using Lavarel framework
* Supports multiple users unlike v3
* Support of HEIC files and subsequently convert raw files (e.g. .NEF) into jpeg
* Ghostbuster command to clean up dead symlinks
* Parse additional xmp sidecars files to update metadata

[2.1.0]
* Use latest base image
* Update PHP to 7.3
* Make php.ini customizable

[2.1.1]
* Update Lychee to 4.0.7
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.0.7)

[2.2.0]
* Update Lychee to 4.0.8
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.0.8)
* fixes #783 : Can't rename tag album
* fixes #781 : Fixes a bug which prevented the use of sharing albums between users
* fixes #779 : Fixes some missing information on Tag Albums in the front end.
* fixes #766 : It is no longer possible to use the 'photo rotation' functionality
* fixes #751 : In some rare instance, it was not possible to generate video thumbnails
* fixes #769 : when moving pictures in Image view, the second try resulted in failure
* fixes : Settings are accessible in Image view
* new #758 : Add the possibility to chose the picture ordering per album

[2.3.0]
* Update Lychee to 4.1.0
* Update PHP to 7.4
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.1.0)
* Passwordless support and Sensitive folders
* #808 : Update traditional Chinese files.
* #813 : some error 500 during installations were not properly caught.
* #806 : Direct Links of albums do not respect url if lychee installed in subdirectory
* #811 : fall back to native metadata extraction on error
* #810 : fix(rss): avoid display feed link in HTML if RSS option is disabled

[2.4.0]
* fixes #831 - Bad extension filename when you upload `*.jpg`
* new #874 - Update CLI Takedate
* new #832 - Major rework of backend
  * start using Livewire for the front-end, for now accessible at example.com/livewire if enabled via LIVEWIRE_ENABLED in .env (DO NOT USE, still in development)
  * use Facade AccessControl to access Session information (basically home-brewed Auth Facade)
  * heavy refactoring of the core, introducing more granularity:
    * Interfaces are Contracts
    * Group Factories
    * use trait on album for smaller dedicated operations
    * add Nested Set theory to Album to allow access to all descendants

[2.4.1]
* Update Lychee to 4.2.1
* new #875 : Add custom cover for albums 

[2.5.0]
* Use base image v3

[2.5.1]
* Update Lychee to 4.2.2
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.2.2)
* fixes #882 : Password albums were broken.
* fixes #891 : Download: file not found on password protected Album
* fixes #895 : Default license display
* fixes #888 : Refactoring of the rotation code
* new #887 : Add the possibility to not display the GPS direction on the map
* new #892 : Add --force option to the Command Line Interface for Takedate
* fixes #890 : Fix delete bug when selecting multiple sub albums
* new #901 : Add more diagnostics checks
* new #905 : Improve Chinese translation

[2.6.0]
* Update Lychee to 4.3.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.3.0)
* new #940 : Improved support for touch devices.
* new #939 : Responsive web design for small screens.
* fixes #959 : Excluded '/api/Session::init' from CSRF protection (as per the API specs).
* fixes #959 : Fixes .lycheeignore support.
* new #942 : Add support for Portuguese language.
* fixes #932 : Public photos hidden 

[2.6.1]
* Update Lychee to 4.3.4
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.3.4)

[2.6.2]
* Update Lychee to 4.3.6
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.3.6)

[2.6.3]
* Update Lychee to 4.4.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.4.0)
* PHP 8.0 is now required

[2.6.4]
* Update base image to 3.2.0

[2.7.0]
* Update Lychee to 4.5.3
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.5.3)
* Use of file streams instead of local file copy (step forward to S3).
* Adding QR code support for sharing
* Better Error handling.
* SQL optimizations and refactoring of the internal architecture and representations of Albums.
* Support of Vietnamese language

[2.8.0]
* Update Lychee to 4.6.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.6.0)
* Provide an ASCII fallback for multibyte filenames by @kamil4 in https://github.com/LycheeOrg/Lychee/pull/1453
* Cleanup download tests by @nagmat84 in https://github.com/LycheeOrg/Lychee/pull/1457
* Run npm install in post-merge hook by @qwerty287 in https://github.com/LycheeOrg/Lychee/pull/1445
* Authorization tests by @nagmat84 in https://github.com/LycheeOrg/Lychee/pull/1414
* Fix CI badge in readme by @d7415 in https://github.com/LycheeOrg/Lychee/pull/1460
* just remove --no-suggest by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1463
* Use Laravel Auth facade. by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1403

[2.8.1]
* Update Lychee to 4.6.1
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.6.1)
* This update contains a Security Update which fix multiple XSS vulnerability and update the Content Security Policy.
* This update will drop the API key in favor to Authorization token. As a result, once the migration is applied the old API token won't work anymore.
* Drop page support by @ildyria in #1489
* Add user and better structure to session json by @nagmat84 in #1443
* Move 'installation:complete' to web instead of per route by @ildyria in #1467
* Cleaning config table by @ildyria in #1491
* Update Exec.php by @corrilan in #1153
* Ensure that Admin rights are overloading others by @ildyria in #1508
* Improve config options by @qwerty287 in #1366
* Fixes #1514 by @nagmat84 in #1515
* Shared albums should not consider the require_link property. by @ildyria in #1480
* Improve API client usability by @qwerty287 in #1368
* Improve & fix CSP by @ildyria in #1528
* Fixes #1506 by @nagmat84 in #1516
* Remove use directive by @nagmat84 in #1537
* i18n(ES) by @joebordes in #1541
* i18n(EN) grammar and syntax detected by Grammarly by @joebordes in #1542
* Fix dead Installation/Build link in readme.md by @PeterDaveHello in #1543
* Typofixes by @nexxai in #1544
* Add an option to regenerate square thumbs by @kamil4 in #1545
* add exec check by @ildyria in #1540
* Add csp exceptions by @kamil4 in #1551

[2.8.2]
* Update Lychee to 4.6.2
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.6.2)
* Allow to set custom env options
* Arrayable DTO using reflection by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1529
* Adopt new modern dialog by @nagmat84 in https://github.com/LycheeOrg/Lychee/pull/1519
* Consolidate localization by @nagmat84 in https://github.com/LycheeOrg/Lychee/pull/1494
* Add option to skip diagnostic checks by @qwerty287 in https://github.com/LycheeOrg/Lychee/pull/1558
* Upgrade to Laravel 9 + switch to Laragear/WebAuthn by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1469
* Add option to append tags by @qwerty287 in https://github.com/LycheeOrg/Lychee/pull/1564
* V4.6.2 by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1574
* fix smart albums rights by @ildyria & @nagmat84 in https://github.com/LycheeOrg/Lychee/pull/1578
* Fix files installation if no Git repo is available by @qwerty287 in https://github.com/LycheeOrg/Lychee/pull/1581

[2.8.3]
* Update Lychee to 4.6.5
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.6.5)
* allow username change by @ildyria in #1667

[2.9.0]
* Update Lychee to 4.7.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.7.0)
* Use Enum to enforce stricter types (warning php 8.1) by @ildyria in #1618
* Nuke demo generator, does not reflect the latest version. by @ildyria in #1673
* Ensure php version is correct prior migrations by @ildyria in #1671
* Customizable album decorations by @evoludolab in #1631
* More robust fix on HasAdminUser by @ildyria in #1684
* Composer update - php-exif 0.8 + bump to version 4.7 by @ildyria in #1680

[2.9.1]
* Update Lychee to 4.7.1
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.7.1)
* Update French.php by @davidbercot in #1688
* Fixes #1674 - Share button not working by @ildyria in #1692
* Fixes #1630 - 2FA not working. by @ildyria in #1691
* Allow to store all files in storage by @qwerty287 in #1650
* Fix 2fa tests by @ildyria in #1698
* Fix cache busting on user.css by @ildyria in #1702
* fix migration when photo do not have an album by @ildyria in #1701
* fix exception missing driver by @ildyria in #1705
* Simple composer update + fix php8.2 warning on formatting. by @ildyria in #1706
* allow forcing https scheme by @ildyria in #1707
* update config doc + sync front + dusting by @ildyria in #1713
* Fixes #1703 by @ildyria in #1709
* Setting to make On This Day smart album public by @aldjordje in #1708
* Fix sync default user by @ildyria in #1722
* Add configuration option to set auth guard driver by @r7l in #1724
* Fix wording for authentication guard option by @r7l in #1730
* Avoid showing errors in tests when they are expected. by @ildyria in #1728
* Fix URLs in RSS feeds (#1732) by @nicokaiser in #1738
* Set theme-color on frontend template (#1740) by @nicokaiser in #1741
* execute custom js from custom by @cshyam1892 in #1697
* migrate locales from 'app/Locale/' to 'lang/', drop Lang Facade. by @ildyria in #1733
* use RuleSets instead of directly rule property by @ildyria in #1726
* DTO → Resources by @ildyria in #1668

[2.9.2]
* Update Lychee to 4.7.2
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.7.2)
* Ship an empty custom.js to prevent spamming the console (#1753) by @nicokaiser in https://github.com/LycheeOrg/Lychee/pull/1755
* Fixes locale not working (#1754) by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1756
* Set noindex for error responses by @nicokaiser in https://github.com/LycheeOrg/Lychee/pull/1758
* Fixes unknown orientation in GdHandler by @wladif in https://github.com/LycheeOrg/Lychee/pull/1759
* Add setting auto_fix_orientation to enable auto image rotation by @wladif in https://github.com/LycheeOrg/Lychee/pull/1766
* Migrate to Laravel 10 by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1764
* Compile WebAuthn.js into frontend.js by @qwerty287 in https://github.com/LycheeOrg/Lychee/pull/1769
* composer update by @qwerty287 in https://github.com/LycheeOrg/Lychee/pull/1773
* Version 4.7.2 by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1772

[2.9.3]
* Update Lychee to 4.7.3
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.7.3)

[2.9.4]
* Update Lychee to 4.7.4
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.7.4)
* Bump guzzlehttp/psr7 from 2.4.4 to 2.5.0 by @dependabot in #1804
* [Enhancement] Make it possible to fetch random image without needing to be public & starred by @mingan666 in #1819

[2.10.0]
* Update Lychee to 4.8.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.8.0)
* Use access permissions instead of `base_album` table to determine access rights. by @ildyria in #1792

[2.10.1]
* Update Lychee to 4.8.1
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.8.1)
* Support very old versions (untestable code) by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1826
* Add backend implementation to use file's last modified time by @wladif in https://github.com/LycheeOrg/Lychee/pull/1821
* Now support last_modified_time when uploading file without exif data.
* Fix left/right in photo view of tag albums by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1828
* Support API documentation by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1827
* Composer update + fix php stan complaints by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1829
* fixes Mass assignment problem in Access Permissions #1833 by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1835

[2.11.0]
* Update Lychee to 4.9.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.9.0)
* Remove public option from album ordering: no longer supported by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1848
* Fix SQL error on removing public status from album by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1850
* Remove homemade Log solution, add support log-viewer by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1846

[2.11.1]
* Update Lychee to 4.9.2
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.9.2)
* Fixes #1855 - update Makefile by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1856
* Fix version 4.9 log-viewer paths hard coding by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1857
* remove hashes from CSP when using log-viewer by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1861
* Remove duplicate link. Tweak comment by @d7415 in https://github.com/LycheeOrg/Lychee/pull/1862

[2.11.2]
* Update Lychee to 4.9.3
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.9.3)
* Add a sample image without extension that will be used in tests. by @wladif in #1863
* feat(#1869): Allow script-src and connect-src configuration by @timo-reymann in #1870
* Support files without extensions in importFromUrl by @wladif in #1873
* Replace URL with LycheeOrg one by @wladif in #1886
* Make fileTimeModified parameter optional in Photo::add by @ildyria in #1887
* Fixes 1853 - license broken by @ildyria in #1889

[2.11.3]
* Update Lychee to 4.9.4
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.9.4)
* Make exceptions in one log line by @ildyria in #1901
* sync Lychee-front by @ildyria in #1906
* Improved speed on global table by @ildyria in #1899
* Update dependencies by @qwerty287 in #1908

[2.12.0]
* Add ftp support

[2.13.0]
* Fix file permissions
* Setup email and laravel runner via cron

[2.14.0]
* Update Lychee to 4.10.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.10.0)
* Fix NSFW not toggling via Protection Panel by @ildyria in #1928
* Improve translations to German by @CodingWithCard in #1933
* Use Actions instead of direct call in controller by @ildyria in #1916
* jobs can now also take string as input (upload in smart albums) by @ildyria in #1919
* Add integrity DB check by @ildyria in #1922
* Support ratio by @ildyria in #1925
* Update OSM domain by @qwerty287 in #1935
* Remove OSM subdomains by @qwerty287 in #1936
* Fixes #1942 "Content-Security-Policy blocks blob requests required for Google Motion Pictures images" by @Merlyn42 in #1943

[2.15.0]
* Update Lychee to 4.11.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.11.0)
* Composer update by @ildyria in #1961
* Change two German translations by @caminsha in #1963
* Use enum for `album_subtitle_type` by @ildyria in #1967
* Fix bad placeholder in PT locale. Fixes #1975. by @d7415 in #1976
* Enable video thumbnail by default by @Lingxi-Li in #1971
* Hungarian language added by @KnauszFerenc in #1977

[2.15.1]
* Update Lychee to 4.11.1
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.11.1)
* How about we don't execute tests twice? by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1982
* Minor fixes on List sharing permissions. by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1981
* fix complaints in Diagnostics when no migrations has been run by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1990
* Fixes #1751 - Add error thrown if APP_URL does not match current url by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1985
* Fixes no log write access infinite loop by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1991
* Fixes #1950 : Do not enforce strict model when downloading by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1997
* Fixes #1686 by providing absolute path if not set by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1998
* Webauthn supports also username by @ildyria in https://github.com/LycheeOrg/Lychee/pull/1999
* version 4.11.1 by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2000

[2.16.0]
* Update Lychee to 4.12.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.12.0)
* Fix missing album decorations by @evoludolab in #2003
* fix `max/min_taken_at by @evoludolab in #2005
* fix missing left-right button on smart albums by @ildyria in #2008
* Better diagnostics by @ildyria in #2010
* Better support for future policies. by @ildyria in #2012
* Replace layout and overlay to proper Enum types by @ildyria in #2015

[2.17.0]
* Update Lychee to 4.13.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v4.13.0)
* Unify photoId to photoID and albumId to albumID when found by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2017
* Unique constraint for config keys by @qwerty287 in https://github.com/LycheeOrg/Lychee/pull/2018
* Update composer (include breaking) by @qwerty287 in https://github.com/LycheeOrg/Lychee/pull/2019
* Cleaning unused files + sync front end by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2020
* Fix complaint due to type casting by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2024
* License as enum type by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2025
* Minor changes in prevision for Livewire. by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2026

[2.18.0]
* Update base image to 4.2.0

[2.19.0]
* Update Lychee to 5.0.1
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.0.0)
* Reworked frontend

[2.19.1]
* Update Lychee to 5.0.2
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.0.2)
* Fixes hover (left-right) preventing clicks on volume etc buttons + fix frame button by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2116
* Fix #2118 - Fix drag upload bug by @maoxian-1 in https://github.com/LycheeOrg/Lychee/pull/2119
* Fix SQL Injection + v5.0.2 by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2123

[2.19.2]
* Update Lychee to 5.0.3
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.0.3)
* fix #2126 by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2127
* Avoid more issues because people can't read release notes... by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2124
* Description should be desc for overlay by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2135
* Provide the ability to change the sorting of sub album per album (Livewire only). by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2128
* better diagnostics by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2142
* fix Russian about. by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2143
* Fix Russian again by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2157
* Fix symbolic urls by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2159
* Update Readme, add theme repository, optimize ImageMagick by @tinohager in https://github.com/LycheeOrg/Lychee/pull/2161
* Fix custom.js not being loaded by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2170
* Fix uploading large number of images fails with 429 by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2169
* Remove text-neutral for easier configuration of themes by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2171
* Add compact view for albums by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2153
* Fix WebAuthn not working by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2155
* fix QR code by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2177
* Fix livewire not working on directory folders by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2150
* Allow different aspect ratios for album thumbs (+ per album setting) by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2138
* Improve diagnostics by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2181
* Fix double f in sidebar by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2187
* Add notify toast when updating user by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2179
* Fix errors on access rights by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2180
* Fix back button on unlock page. by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2178
* Add left-right for login button + add custom go Home button by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2185
* make APP_URL optional again by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2182

[2.20.0]
* Update Lychee to 5.1.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.1.0)
* Also load Thumb/Thumb2x as fail-over when Small does not exist by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2197
* display owner names instead of Shared Albumns by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2201
* add fallback on small2x for header if medium does not exists by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2199
* Improve token guard to not crash when provided with Basic Auth by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2193
* Improve diagnostics with count of thumbs that can be regenerated by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2200
* Fixes star/unstar on right click by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2204
* Oauth is now available by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2190

[2.20.1]
* Update Lychee to 5.1.1
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.1.1)
* Bump vite from 4.5.1 to 4.5.2 by @dependabot in https://github.com/LycheeOrg/Lychee/pull/2216
* Fix Diagnostics not visible when migrations are pending by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2210
* Fix import from Dropbox from Livewire side by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2217
* Fix upload on smart albums by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2218
* Fix search broken when hitting albums by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2219
* Add back Download and full size in photo view. by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2220
* Anonymize the paths in the diagnostics by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2226
* Fix title not being updated by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2225
* Add redirection for legacy links by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2227
* Add missing secure header for redirection by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2232

[2.20.2]
* symlink custom.js

[2.21.0]
* Update Lychee to 5.2.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.2.0)

[2.21.1]
* Update Lychee to 5.2.2
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.2.2)
* Fixes HTTP 500 "Attempt to read property "photo_id" on null" on album page when no photo is found for header by @nanawel in https://github.com/LycheeOrg/Lychee/pull/2396
* Use portrait when landscape is not available for header image by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2397

[2.22.0]
* Update Lychee to 5.3.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.3.0)
* Fix description empty string creating bug in layout by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2400
* Add S3 bucket support. by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2379
* Fix teapot not flagging on phpinfo.php by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2411
* Fix migration order by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2410
* Add migration on forgotten license CC-BY-SA by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2416
* Adjusted with suggested CSS by @ildyria in https://github.com/LycheeOrg/Lychee/pull/2413
* Adding copyright to albums #1838 by @ThanasisMpalatsoukas in https://github.com/LycheeOrg/Lychee/pull/1880

[2.22.1]
* Update Lychee to 5.3.1
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.3.1)

[2.23.0]
* Update Lychee to 5.4.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.4.0)

[2.24.0]
* Update Lychee to 5.5.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.5.0)

[2.24.1]
* Update Lychee to 5.5.1
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v5.5.1)

[2.25.0]
* Update Lychee to 6.0.0
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v6.0.0)
* Improved User Interface: A more responsive and modern frontend, providing an improved better user experience.
* Slideshow Mode: A new slideshow mode has been added, allowing you to view your photos in a more dynamic way. Just press the space bar or click the play button in the header to start the slideshow.
* Dark and Light themes: By popular demand, we have added the ability to chose between a dark and light themes. No more fiddling with CSS files!
* Upload by chunk: Be free of the limitation imposed by your server. Large files are now automatically split into smaller chunks at your convenience.
* Introducing Lychee SE

[2.25.1]
* Update Lychee to 6.0.1
* [Full changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v6.0.1)
* Fix icons when using sub-folder install by @​ildyria in https://github.com/LycheeOrg/Lychee/pull/2607
* Fix login menu not visible when set to the right. by @​ildyria in https://github.com/LycheeOrg/Lychee/pull/2605
* [StepSecurity] Apply security best practices by @​step-security-bot in https://github.com/LycheeOrg/Lychee/pull/2609
* fix dependencies by @​ildyria in https://github.com/LycheeOrg/Lychee/pull/2626
* Fix link to Logs in case of sub folder hosting by @​ildyria in https://github.com/LycheeOrg/Lychee/pull/2624
* Add link to album from sharing page by @​ildyria in https://github.com/LycheeOrg/Lychee/pull/2625 

[2.26.0]
* Update Lychee to 6.1.0
* [Full Changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v6.1.0)
* `new` [#&#8203;2629](https://github.com/LycheeOrg/Lychee/issues/2629) : Get automated signed releases by [@&#8203;ildyria](https://github.com/ildyria).
* `fixes` [#&#8203;2630](https://github.com/LycheeOrg/Lychee/issues/2630) : Fix sub-albums sorting not being respected per album by [@&#8203;ildyria](https://github.com/ildyria).
* `new` [#&#8203;2633](https://github.com/LycheeOrg/Lychee/issues/2633) : Enable lazy loading for pictures past number 10 in an album by [@&#8203;ildyria](https://github.com/ildyria).
* `fixes` [#&#8203;2634](https://github.com/LycheeOrg/Lychee/issues/2634) : Fix building artifact in Integrate workflow by [@&#8203;d7415](https://github.com/d7415).
* `new` [#&#8203;2636](https://github.com/LycheeOrg/Lychee/issues/2636) : Vite local dev by [@&#8203;ildyria](https://github.com/ildyria).

[2.26.1]
* Update Lychee to 6.1.1
* [Full Changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v6.1.1)
* Fix Authentik icon by [@&#8203;sushain97](https://github.com/sushain97) in https://github.com/LycheeOrg/Lychee/pull/2738
* Fix wrong config in album by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2741
* Fix create Tag album callback not triggering by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2743

[2.26.2]
* Update Lychee to 6.1.2
* [Full Changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v6.1.2)
* Avoid preventing rendering when albums is returning 401 by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2745
* Fix photo timeline when there are no borders by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2746
* Create automated Signed releases. by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2749
* ListAlbum fix by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2753
* version 6.1.2 by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2747

[2.27.0]
* Update Lychee to 6.2.0
* [Full Changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v6.2.0)
* Change the plaintext-field to a password-field in AlbumUnlock by [@&#8203;Gendra13](https://github.com/Gendra13) in https://github.com/LycheeOrg/Lychee/pull/2757
* Documentation stuff by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2756
* Fix error appearing when clicking on + menu in Smart albums by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2759
* Add password unlocking middleware by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2761
* Fix copy move selection by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2760

[2.28.0]
* Update Lychee to 6.3.1
* [Full Changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v6.3.1)
* 6.3.1: catch Error when Redis is crashing and not die in 500... by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3006
* Composer + npm update by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2824
* Fix user count not updated on front-end when creating new users by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2823
* Update README.md by [@&#8203;tinohager](https://github.com/tinohager) in https://github.com/LycheeOrg/Lychee/pull/2825
* Optimize User Management by [@&#8203;tinohager](https://github.com/tinohager) in https://github.com/LycheeOrg/Lychee/pull/2827
* Remove Keyboard Navigation help view on mobile by [@&#8203;tinohager](https://github.com/tinohager) in https://github.com/LycheeOrg/Lychee/pull/2830
* Fix dialog width for mobile  by [@&#8203;tinohager](https://github.com/tinohager) in https://github.com/LycheeOrg/Lychee/pull/2829
* Fixes upload fails for non-existent partner video upload to S3 by [@&#8203;kiancross](https://github.com/kiancross) in https://github.com/LycheeOrg/Lychee/pull/2849
* Fix functionality to hide back button when configuration is set by [@&#8203;kiancross](https://github.com/kiancross) in https://github.com/LycheeOrg/Lychee/pull/2852
* Fix aesthetics of footer social icons by [@&#8203;kiancross](https://github.com/kiancross) in https://github.com/LycheeOrg/Lychee/pull/2851
* Add scroll to top when pressing the 'i' or clicking on details by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2850
* Exit with error for unsupported S3 backend by [@&#8203;kiancross](https://github.com/kiancross) in https://github.com/LycheeOrg/Lychee/pull/2856
* Force redirection if accessing urls where being logged is required by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2846
* Fix webauthn not showing up by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2858
* Minor UI improvements by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2860
* ...

[2.28.1]
* Update Lychee to 6.3.2
* [Full Changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v6.3.2)
* Fix wrong translation keys. by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3007
* RSS Feed Enhancements (enclosure + category) by [@&#8203;cdzombak](https://github.com/cdzombak) in https://github.com/LycheeOrg/Lychee/pull/3009
* Fix missing dependency in production build by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3010
* [@&#8203;cdzombak](https://github.com/cdzombak) made their first contribution in https://github.com/LycheeOrg/Lychee/pull/3009

[2.28.2]
* Update Lychee to 6.3.3
* [Full Changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v6.3.3)
* Do not display useless message in diagnostics by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3015
* Handle failing encoding gracefully. by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3011
* Add config option for the number of operations done by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3012
* Full permission check is now disabled by default. by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3013
* \[SE] Add more privacy options by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3014
* Add more statistics: punch card a la GitHub (SE). by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/2983
* Fix aspect ratio being dropped due to Tree shaking by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3032
* Add more markdown processing options by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3026
* Offboarding Sonar - done with it breaking randomly... by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3028
* Version 6.3.3 by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3033

[2.29.0]
* Update to correct base image 5.0.0

[2.29.1]
* Update Lychee to 6.3.4
* [Full Changelog](https://github.com/LycheeOrg/Lychee/releases/tag/v6.3.4)
* Add Diagnostics cache checks by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3027
* Scroll top button for album and albums views by [@&#8203;sancsin](https://github.com/sancsin) in https://github.com/LycheeOrg/Lychee/pull/3035
* Split-oauth endpoint to reduce the number of requests. by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3023
* Simplified Chinese localization by [@&#8203;x1ntt](https://github.com/x1ntt) in https://github.com/LycheeOrg/Lychee/pull/3046
* Version 6.3.4 by [@&#8203;ildyria](https://github.com/ildyria) in https://github.com/LycheeOrg/Lychee/pull/3047

