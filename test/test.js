#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const USERNAME = 'testuser';
    const PASSWORD = 'testpassword';
    const ALBUM_NAME = 'testalbum';
    const TEST_IMAGE_LINK = 'https://www.cloudron.io/img/screenshots/cloudron-hero.png';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function createUser() {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(USERNAME);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(PASSWORD);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="password_confirmation"]')).sendKeys(PASSWORD);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//a[contains(text(), "Open Lychee")]'));
        await browser.sleep(2000);
    }

    async function closePopup() {
        await waitForElement(By.xpath('//button[contains(., "Close")]'));
        await browser.findElement(By.xpath('//button[contains(., "Close")]')).click();
    }

    async function login() {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//button[contains(., "Sign")]'));
        await waitForElement(By.id('username'));
        await browser.sleep(3000);
        await browser.findElement(By.id('username')).sendKeys(USERNAME);
        await browser.findElement(By.id('password')).sendKeys(PASSWORD);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[contains(., "Sign")]')).click();
        await browser.sleep(5000);
    }

    async function imageExists() {
        await browser.get(`https://${app.fqdn}/gallery/unsorted`);
        await waitForElement(By.className('photo'));
    }

    async function createAlbum() {
        await browser.get(`https://${app.fqdn}`);
        await closePopup();
        await waitForElement(By.xpath('//span[contains(@class, "pi-plus")]/ancestor::button'));
        await browser.findElement(By.xpath('//span[contains(@class, "pi-plus")]/ancestor::button')).click();
        await browser.sleep(4000);
        await waitForElement(By.xpath('//span[contains(., "New Album")]/ancestor::a'));
        await browser.findElement(By.xpath('//span[contains(., "New Album")]/ancestor::a')).click();
        await waitForElement(By.id('title'));
        await browser.findElement(By.id('title')).sendKeys(ALBUM_NAME);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[contains(.,"Create Album")]')).click();
        await browser.sleep(4000);
    }

    async function uploadImage() {
        await browser.get(`https://${app.fqdn}`);
        await closePopup();
        await waitForElement(By.xpath('//span[contains(@class, "pi-plus")]/ancestor::button'));
        await browser.findElement(By.xpath('//span[contains(@class, "pi-plus")]/ancestor::button')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//span[contains(., "Import from Link")]/ancestor::a'));
        await browser.findElement(By.xpath('//span[contains(., "Import from Link")]/ancestor::a')).click();
        await browser.sleep(2000);

        await waitForElement(By.id('links'));
        await browser.findElement(By.id('links')).sendKeys(TEST_IMAGE_LINK);
        await browser.findElement(By.xpath('//button[contains(., "Import")]')).click();
        await browser.sleep(4000);

        await browser.get(`https://${app.fqdn}/gallery/unsorted`);
        await waitForElement(By.className('photo'));
    }

   xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
   it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

   it('can get app information', getAppInfo);
   it('can create user', createUser);
   it('clear cache', clearCache);
   it('can login', login);
   it('can create album', createAlbum);
   it('can upload image', uploadImage);
   it('image exists', imageExists);

   it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });

   it('restore app', function () {
       const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
       execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
       execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
       getAppInfo();
       execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
   });

   it('clear cache', clearCache);
   it('can login', login);
   it('image exists', imageExists);
   it('clear cache', clearCache);

   it('move to different location', async function () {
       browser.manage().deleteAllCookies();

       // ensure we don't hit NXDOMAIN in the mean time
       await browser.get('about:blank');

       execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
       getAppInfo();
   });

   it('clear cache', clearCache);
   it('can login', login);
   it('image exists', imageExists);

   it('uninstall app', async function () {
       // ensure we don't hit NXDOMAIN in the mean time
       await browser.get('about:blank');
       execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
   });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id com.electerious.lychee.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('clear cache', clearCache);
    it('can get app information', getAppInfo);
    it('can create user', createUser);
    it('clear cache', clearCache);
    it('can login', login);
    it('can create album', createAlbum);
    it('can upload image', uploadImage);
    it('image exists', imageExists);

    it('clear cache', clearCache);
    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('can login', login);
    it('can upload image', uploadImage);
    it('image exists', imageExists);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
