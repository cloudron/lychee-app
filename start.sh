#!/bin/bash

set -eu

readonly mysql="mysql -u ${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} -h ${CLOUDRON_MYSQL_HOST} --port ${CLOUDRON_MYSQL_PORT} --database ${CLOUDRON_MYSQL_DATABASE}"

readonly ARTISAN="sudo -E -u www-data php /app/code/artisan"

echo "=> Ensure directories"
mkdir -p /app/data/{data,sym} /app/data/uploads/{big,import,medium,thumb,small} /run/lychee/{sessions,bootstrap-cache,framework-cache,logs}

table_count=$($mysql -NB -e "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '${CLOUDRON_MYSQL_DATABASE}';" 2>/dev/null)

rm -f /app/data/.initialized

if [[ "${table_count}" == "0" ]]; then
    echo "=> First run"
    mkdir -p /app/data/storage
    cp -R /app/code/storage.template/* /app/data/storage
    cp /app/pkg/env.production.template /app/data/env

    touch /app/data/user.css /app/data/custom.js

    chown -R www-data:www-data /run/lychee /app/data

    echo "=> Generating app key"
    $ARTISAN key:generate --no-interaction --force
else
    chown -R www-data:www-data /run/lychee /app/data
fi

# remove after next release
touch /app/data/custom.js

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

[[ -z "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-}" ]] && export CLOUDRON_MAIL_FROM_DISPLAY_NAME="Lychee"

# laravel is configured to required these perms . see config/filesystem.php (https://blog.sjorso.com/how-to-set-correct-laravel-file-permissions)
find /app/data/uploads -type d -exec chmod 2775 {} \;
find /app/data/sym -type d -exec chmod 2775 {} \;

echo "=> Run migrations and seed database"
$ARTISAN migrate --no-interaction --force

# sessions, logs and cache
[[ -d /app/data/storage/framework/sessions ]] && rm -rf /app/data/storage/framework/sessions
ln -sf /run/lychee/sessions /app/data/storage/framework/sessions
rm -rf /app/data/storage/framework/cache && ln -s /run/lychee/framework-cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/lychee/logs /app/data/storage/logs

echo "=> Fixup permissions"
chown -R www-data.www-data /app/data /run/lychee

# https://github.com/LycheeOrg/Lychee/issues/1059
$ARTISAN view:clear

echo "=> Start apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
