FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=LycheeOrg/Lychee versioning=semver extractVersion=^v(?<version>.+)$
ARG LYCHEE_VERSION=6.3.4

# get lychee source from the release zip (includes prebuild assets)
RUN cd /tmp && \
    wget https://github.com/LycheeOrg/Lychee/releases/download/v${LYCHEE_VERSION}/Lychee.zip && \
    unzip Lychee.zip && mv /tmp/Lychee/* /app/code/ && \
    rm -rf Lychee.zip /tmp/Lychee

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/lychee.conf /etc/apache2/sites-enabled/lychee.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN a2enmod rewrite php8.3 && a2dismod perl

# configure php
RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 500M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_size 500M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 500M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP user_agent "\"Lychee/4 (https://lycheeorg.github.io/)\"" && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.save_path /run/lychee/sessions && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_divisor 100

# better to have these configurable via php.ini
RUN sed -e '/max_execution_time\|post_max_size\|upload_max_filesize\|max_file_uploads/d' -i /app/code/public/.htaccess

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

RUN mv /app/code/storage /app/code/storage.template && ln -s /app/data/storage /app/code/storage \
    && mv /app/code/bootstrap/cache /app/code/bootstrap/cache.template && ln -s /run/lychee/bootstrap-cache /app/code/bootstrap/cache \
    && ln -s /app/data/env /app/code/.env

# https://github.com/LycheeOrg/Lychee/blob/master/config/filesystems.php (sym explanation)
RUN rm -rf /app/code/public/dist/user.css && ln -s /app/data/user.css /app/code/public/dist/user.css && \
    rm -rf /app/code/public/dist/custom.js && ln -s /app/data/custom.js /app/code/public/dist/custom.js && \
    rm -rf /app/code/public/uploads && ln -s /app/data/uploads /app/code/public/uploads && \
    rm -rf /app/code/public/sym && ln -s /app/data/sym /app/code/public/sym

RUN chown -R www-data.www-data /app/code

ADD env.production.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
